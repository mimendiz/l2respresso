import ROOT

def compute_ratio(num, den):
    eff = num.Clone("eff")
    for i in range(eff.GetNbinsX()):
        content_num = num.GetBinContent(i+1)
        error_num = num.GetBinError(i+1)
        content_den = den.GetBinContent(i+1)
        content=0
        error=0
        if content_den!=0:
            content = content_num/content_den
            error =  error_num/content_den
        eff.SetBinContent(i+1, content)
        eff.SetBinError(i+1, error)
    return eff




f = ROOT.TFile.Open("L2Res_PFJetEff.root", "READ")
hlt_path = "PFJet"
pt_bin = ["40", "60", "80", "140", "200","260", "320", "400", "450", "500"]
outfile = ROOT.TFile.Open("PFJetEff.root", "RECREATE")

for pt in pt_bin:
        num = f.Get(hlt_path+pt+"Num")
        den = f.Get(hlt_path+pt+"Den")
        den.Write()
        num.Write()
        ratio = compute_ratio(num, den)
        ratio.SetName(hlt_path+pt+"Eff")
        ratio.Write()

outfile.Close()
















