import os, sys

import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, BaseSchema
from coffea import processor, hist, util
from coffea.nanoevents.methods import vector
from coffea.nanoevents.methods.vector import ThreeVector

import uproot

import array
import json
import numpy as np

import time

from hist import Hist
import hist

import matplotlib.pyplot as plt
import mplhep

from processors import triggerefficiency

fileset = {
    'JetHT':[
        #'/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/UL2016_MiniAODv2_JMENanoAODv9-v1/2430000/7E050CC8-BC9A-A447-8809-216421F59AE9.root',
        '/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/HIPM_UL2016_MiniAODv2_JMENanoAODv9-v1/2430000/A1CFE3CE-A38B-3D48-971D-AA587CBD2B33.root',
    ]
}

out = processor.run_uproot_job(
    fileset,
    "Events",
    triggerefficiency.L2ResProcessor(),
    executor=processor.iterative_executor,
    executor_args={
      "schema": NanoAODSchema,
    },
    #maxchunks=1000,
)

fout = uproot.recreate('L2Res_PFJetEff.root')
trigger_method = "histo1"
histogram = out[trigger_method]
histogram = histogram.to_hist()

triggers = histogram.axes['trigger']
print(triggers)
for trigger in triggers:
  hden_emulation = histogram[{"trigger":trigger , "fraction":"Den", "method": "Emulation"}].project("jet_pt")
  hden_tag_probe = histogram[{"trigger":trigger , "fraction":"Den", "method": "Tag&Probe"}].project("jet_pt")
  hnum_emulation = histogram[{"trigger":trigger, "fraction":"Num", "method":"Emulation"}].project("jet_pt")
  hnum_tag_probe = histogram[{"trigger":trigger, "fraction":"Num", "method":"Tag&Probe"}].project("jet_pt")

  fout["hEmulation"+trigger+"Den"] = hden_emulation
  fout["hEmulation"+trigger+"Num"] = hnum_emulation
  fout["hTag&Probe"+trigger+"Den"] = hden_tag_probe
  fout["hTag&Probe"+trigger+"Num"] = hnum_tag_probe

fout.close()
