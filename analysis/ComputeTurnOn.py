import uproot
import matplotlib.pyplot as plt
import mplhep
import scipy
import numpy as np
from scipy.optimize import curve_fit

def mod_error_funct(pt, a, mu, sigma):
    return a + 0.5* (1-a) * (1 + scipy.special.erf((pt - mu)/sigma))

def compute_eff_fit(heff, trigger_pt):
    ''' Take bin contents and bin centers for the fit'''
    bin_centers = h.axes[0].centers
    bin_contents= h.values()
    '''
        In order to improve the fit make the fit range smaller
        around -30GeV and +300 GeV around the trigger pT
    '''
    fit_range_min = (np.abs(bin_centers-trigger_pt+30)).argmin()
    fit_range_max = (np.abs(bin_centers-trigger_pt-100)).argmin()
    fit_range = bin_centers[fit_range_min:fit_range_max]
    fit_content= bin_contents[fit_range_min:fit_range_max]
    '''
        For the fit to perform well we need to give some initial 
        values
        p0 = {
        a     = 0.0001,
        mu    = trigger pt,
        sigma = 30
        }
    '''
    p0 = [0.0001, trigger_pt, 30]
    ''' 
        For the fit of the efficiency we use a modified version
        of the error function
    '''
    popt, pcov = curve_fit(mod_error_funct, fit_range, fit_content, p0)
    perr = np.sqrt(np.diag(pcov))
    return popt, perr

def compute_turn_on(popt, perr):
    var = [-1., 0., 1.]
    turn_on_errors =  np.zeros((len(var), len(var), len(var)))
    for i1 in range(len(popt)):
        par1_var = popt[0]*(1+var[i1]*perr[0])
        for i2 in range(len(popt)):
             par2_var =  popt[1]*(1+var[i2]*perr[1])
             for i3 in range(len(popt)):
                 par3_var= popt[2]*(1+var[i3]*perr[2])
                 turn_on_err =  scipy.special.erfinv((2*0.99 - 1 - par1_var) / (1-par1_var)) * par3_var + par2_var
                 turn_on_errors[i1, i2, i3] = turn_on_err
    turn_on_min = turn_on_errors.min()
    turn_on_max = turn_on_errors.max()
    turn_on = scipy.special.erfinv((2*0.99 - 1 - popt[0]) / (1-popt[0])) * popt[2] + popt[1]

    return turn_on, turn_on_min, turn_on_max


from matplotlib import rc

hlt_path = "PFJet"
pt_bin = ["40", "60", "80","140", "200", "260", "320", "400", "450", "500"]
patrick = ["no", "no", "no", "no","no", "330", "395", "468", "507", "No data"]
f = uproot.reading.open("PFJetEff.root")
print("file open")
#fig = plt.figure()
#ax = fig.add_subplot()
for i in range(len(pt_bin)):
        fig = plt.figure()
        ax = fig.add_subplot()
        h = f[hlt_path+pt_bin[i]+"Eff"].to_hist()
        content, bins = h.to_numpy()
        err = f[hlt_path+pt_bin[i]+"Eff"].errors()
        par, parerr = compute_eff_fit(h, float(pt_bin[i]))
        turn_on = compute_turn_on(par, parerr)
        print('Turn-on point of the trigger HLT_PFJet'+pt_bin[i], turn_on[0], 'GeV',
            'and Patrick: '+patrick[i])
        print('Min and max:', turn_on[1], 'and', turn_on[2])
        bincenters = 0.5*(bins[1:]+bins[:-1])
        ax.errorbar(bincenters, content, yerr=err, color='black', fmt='none')
        mplhep.histplot(h, color='black', histtype="errorbar", label="data")
        ax.plot(h.axes[0].centers, mod_error_funct(h.axes[0].centers, *par),
            color='red', label = 'ModErf($p_{T}^{j_{1}}$)')
        ax.legend()
        #mplhep.cms.lumitext('Run2016F')
        mplhep.cms.label(data=True, year=2016, rlabel="RunF, 2016 (13 TeV)")
        plt.vlines(x = turn_on[0], ymin = 0, ymax = 0.99, colors = 'purple',
                   label = 'Turn-on point', linestyles='dashed')
        tex= "Turn-on HLT_PFJet"+pt_bin[i]+"="+str(int(turn_on[0]))+' GeV'
        ax.text(turn_on[0]+10 ,1/2, tex)
        plt.xlim(int(pt_bin[i])-20, turn_on[0]+100)
        plt.ylim(0,1.5)
        plt.savefig('../efficiencies/figures/HLT_'+hlt_path+pt_bin[i]+'Efficiency.pdf')

f.close()
