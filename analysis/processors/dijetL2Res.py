import os, sys

import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, BaseSchema
from coffea import processor, hist, util
from coffea.nanoevents.methods import vector
from coffea.nanoevents.methods.vector import ThreeVector

import array
import json
import numpy as np
import random

from tools import tools

def dijet_selection (events, phase_space):
      
    # We only take events with 2 or more jets 
    njets = ak.num(jets)
    events = events[(njets >= phase_space.min_njets)] 
   
    # Define jets
    selectedjets = events.Jet

    # Condition for barrel jet
    cond_lead_jet_barrel = np.abs(selectedjets[:0].eta)) < phase_space.max_abseta_centraljet
    cond_sub_jet_barrel = np.abs(selectedjets[:1].eta)) < phase_space.max_abseta_centraljet
    
    # Take events with at least 1 barrel jet
    events_with_barrel_jet = events[cond_lead_jet_barrel | cond_sub_jet_barrel]
    selectedjets = events_with_barrel_jet.Jet 
    
    # Definition of barrel and probe jets
    # We define a random number (0 or 1), for the case where the two leading
    # jets are in the barrel, to randomly tag the barrel and the probe jet
    nrand = random.randint(2, size = len(events_with_barrel_jet))
    
    barrel_jet = ak.where(cond_lead_jet_barrel & cond_sub_jet_barrel, ak.where(nrand == 0, selectedjets[:,0], selectedjets[:,1]),
                                                                      ak.where(cond_lead_jet_barrel, selectedjets[:,0], selectedjets[:,1])
                         )
    events_with_barrel_jet["BarrelJet"] = barrel_jet

    probe_jet = ak.where(cond_lead_jet_barrel & cond_sub_jet_barrel, ak.where(nrand == 1, selectedjets[:,0], selectedjets[:,1]), 
                                                                     ak.where(cond_lead_jet_barrel, selectedjets[:,1], selectedjets[:,0])
                         )
    events_with_barrel_jet["ProbeJet"] = probe_jet
    
    # Delta_phi between the barrel jet and the probe jet
    delta_phi = tools.delta_phi(barrel_jet.phi, probe_jet.phi)
    cond_delta_phi = delta_phi > phase_space.min_dphi_leadingjets

    # pT balance between the barrel jet and the probe jet
    pT_balance = tools.pT_balance(barrel_jet.pt, probe_jet.pt)
    cond_pT_balance = pT_balance < phase_space.max_pT_balance
   
    # pT_average_cosh(eta_probe)  
    pTave_cosh_eta_probe = tools.pTave_cosh_eta(barrel_jet.pt, probe_jet.pt)
    cond_pTave_cos_eta_probe = pTave_cosh_eta_probe > phase_space.min_pTave_cosh_eta_probe
    
    
    # We apply all the conditions
    events_after_selection = events_with_barrel_jet[cond_delta_phi & cond_pT_balance & cond_pTave_cos_eta_probe]
    
    # We compute alpha:
    alpha = tools.alpha_jets(events_after_selection)   
    events_after_selection['alpha'] = alpha
    
    # We return events
    dijetDict = {'events': events_after_selection} 

    return dijetDict
