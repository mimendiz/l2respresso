import json
from types import SimpleNamespace
# Open confiuration file

class Config():
    def __init__(self):
        config_file = open('config.json')

        # Load json file into dictionary
        configuration = json.load(config_file)


        # Load triggers into tuples with the name in the first entry and the nominal
        # value on the second
        trigger_info = configuration['TriggerMaps']
        self.central_triggers = list(zip(['PFJet'+ s  for s in trigger_info['PFJet']] , 
                                         [int(s) for s in  trigger_info['PFJet']] ))
        self.forward_triggers = list(zip(['PFJetFwd' + s  for s in trigger_info['PFJetFwd']] ,
                                         [int(s) for s in  trigger_info['PFJetFwd']] ))

        self.phase_space_trigger_efficiency = SimpleNamespace(**configuration['PhaseSpace']['TriggerEfficiency'])




