import os, sys

import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, BaseSchema
from coffea import processor, hist, util
from coffea.nanoevents.methods import vector
from coffea.nanoevents.methods.vector import ThreeVector

import array
import json
import numpy as np

# Based on decafMET gammaJes.py
# https://github.com/dvannerom/decafMET/blob/master/decafMET/analysis/processor/gammaJets.py

def tagandprobeSelection(events, phase_space):
  # Apply trigger
  #events = events[HLT_PFJet(events)>0]

  # Make an entry in events for HLT jets, in NanaADO HLT reconstructions is
  # saved as TrigObj, then jets corresponding to HLT reconstruction have an ID
  # equal to 1
  events["HLTJet"] = events.TrigObj[ak.where(events.TrigObj.id==1,True, False)] 
  
  # Only take jets that are tight in the event
  selectedJets = events.Jet
  selectedJets = selectedJets[(events.Jet.isTight)]

  # Define HLT jet selection
  selectedHLTJets = events.HLTJet

  # Take events that contain 2 or more HLT jets and PF jets 
  events =  events[(ak.num(selectedJets)>=phase_space.min_njets)  & (ak.num(selectedHLTJets)>=phase_space.min_njets)]

  # Define alpha for PF reconstruction
  njetsPF = ak.num(events.Jet)
  alphaPF = np.zeros(len(events))
  alphaPF[njetsPF>2] = 0.5 * events[njetsPF>2].Jet[:,2].pt / (events[njetsPF>2].Jet[:,0].pt + events[njetsPF>2].Jet[:,1].pt)
  
 
  # Define alpha for HLT reconstruction
  njetsHLT = ak.num(events.HLTJet)
  alphaHLT = np.zeros(len(events))
  alphaHLT[njetsHLT>2] = 0.5 * events[njetsHLT>2].HLTJet[:,2].pt / (events[njetsHLT>2].HLTJet[:,0].pt + events[njetsHLT>2].HLTJet[:,1].pt)

  # Take events with alpha PF and HLT below 0.3
  events = events[(alphaPF < phase_space.max_alpha) & (alphaHLT< phase_space.max_alpha)]  

  # Define leading PF jet Lorentz vector
  leadingPFJet = events.Jet[:,0]
  leadingPFJetLorentz = ak.zip(
      {
          "pt"  : leadingPFJet.pt,
          "eta" : leadingPFJet.eta,
          "phi" : leadingPFJet.phi,
          "mass": leadingPFJet.mass,
      },
      with_name="PtEtaPhiMLorentzVector"
  )

  # Define sub-leading PF jet Lorentz vector
  subleadingPFJet = events.Jet[:,1]
  subleadingPFJetLorentz = ak.zip(
      {
          "pt"  : subleadingPFJet.pt,
          "eta" : subleadingPFJet.eta,
          "phi" : subleadingPFJet.phi,
          "mass": subleadingPFJet.mass,
      },
      with_name="PtEtaPhiMLorentzVector"
  )

  # Define leading HLT jet Lorentz vector  
  leadingHLTJet = events.HLTJet[:,0]
  leadingHLTJetLorentz = ak.zip(
      {
          "pt"  : leadingHLTJet.pt,
          "eta" : leadingHLTJet.eta,
          "phi" : leadingHLTJet.phi,
          "mass": 0,
      },
      with_name="PtEtaPhiMLorentzVector"
  )

  # Define sub-leading HLT jet Lorentz vector
  subleadingHLTJet = events.HLTJet[:,1]
  subleadingHLTJetLorentz = ak.zip(
      {
          "pt"  : subleadingHLTJet.pt,
          "eta" : subleadingHLTJet.eta,
          "phi" : subleadingHLTJet.phi,
          "mass": 0,
      },
      with_name="PtEtaPhiMLorentzVector"
  )

  # Define PF tag and probe jets using random a random number
  rand_tag = np.random.randint(2, size=len(events)) 
  
  tagPFJet = ak.where(rand_tag==1, leadingPFJet, subleadingPFJet)
  tagPFJetLorentz = ak.where(rand_tag==1, leadingPFJetLorentz, subleadingPFJetLorentz)
  

  probePFJet = ak.where(rand_tag==0, leadingPFJet, subleadingPFJet)
  probePFJetLorentz = ak.where(rand_tag==0, leadingPFJetLorentz, subleadingPFJetLorentz) 

  # Match in DeltaR leading and sub-leading HLT jets to the tag and probe PF
  # jets

  # Compute DeltaR between the Tag PF jet and the leading HLT jets
  deltaR_HLTjet0_tagPFJet = tagPFJetLorentz.delta_r(leadingHLTJetLorentz)
  deltaR_HLTjet1_tagPFJet = tagPFJetLorentz.delta_r(subleadingHLTJetLorentz)
  
  # Compute DeltaR between the Probe PF jet and the leading HLT jets
  deltaR_HLTjet0_probePFJet = probePFJetLorentz.delta_r(leadingHLTJetLorentz)
  deltaR_HLTjet1_probePFJet = probePFJetLorentz.delta_r(subleadingHLTJetLorentz)

  # Only take events where HLT jets are matched to the tag and probe PF jets
  is_event_matched0 = (deltaR_HLTjet0_tagPFJet < phase_space.max_dr_hlt_pf_jet) & (deltaR_HLTjet1_probePFJet< phase_space.max_dr_hlt_pf_jet) 
  is_event_matched1 = (deltaR_HLTjet1_tagPFJet < phase_space.max_dr_hlt_pf_jet) & (deltaR_HLTjet0_probePFJet< phase_space.max_dr_hlt_pf_jet)
  is_event_matched = is_event_matched0 * is_event_matched1
  
  # remove envets that are not matched
  events = events[is_event_matched0 | is_event_matched1]

  #Include leading and subleading HLT to the events
  tagPFJet = tagPFJet[is_event_matched0 | is_event_matched1]
  probePFJet = probePFJet[is_event_matched0 | is_event_matched1]
  leadingHLTJet = leadingHLTJet[is_event_matched0 | is_event_matched1]
  subleadingHLTJet = subleadingHLTJet[is_event_matched0 | is_event_matched1]


  # Match tag and probe to leading and subleading jets
  is_event_matched0_aux = is_event_matched0[is_event_matched0 | is_event_matched1]
  is_event_matched1_aux = is_event_matched1[is_event_matched0 | is_event_matched1]
  tagHLTJet = ak.where(is_event_matched0_aux, leadingHLTJet, subleadingHLTJet)
  probeHLTJet = ak.where(is_event_matched0_aux, subleadingHLTJet, leadingHLTJet)
 
  # Define delta phi between tag and probe jets at HLT and PF level
  dphi_tag_probe_pf = np.abs(tagPFJet.phi - probePFJet.phi)
  dphi_tag_probe_hlt = np.abs(tagHLTJet.phi - probeHLTJet.phi)
  
  # Apply cuts to the events
  events['TagHLTJet'] = tagHLTJet
  events['ProbeHLTJet'] = probeHLTJet
  events['TagPFJet'] = tagPFJet
  events['ProbePFJet'] = probePFJet

  events = events[(dphi_tag_probe_pf>phase_space.min_dphi_leadingjets) & (dphi_tag_probe_hlt>phase_space.min_dphi_leadingjets)]
 
  tagandprobeDict = {'events': events}
  
  return tagandprobeDict
