"""
Collection of common functions used in the l2respresso analysis
"""
# Function to calculate alpha

def alpha_jets(events):
    # We calculate the number of events 
    njets = ak.num(events.Jet)
    alpha = np.zeros(len(events))
    alpha[njets>2] = 0.5 * events[njets>2].Jet[:,2].pt / (events[njets>2].Jet[:,0].pt + events[njets>2].Jet[:,1].pt)

    return alpha

# Function to assign leading / subleading jets randomly

def rand_jets_assign(events, leading_jet, subleading_jet):
    # Create a random number 0 or 1
    nrand = random.randint(2, size = len(events))

    # We assign leading jet and subleading jet randomly
    assigned_leading_jet = ak.where(nrand == 0,  leading_jet, subleading_jet)
    assigned_subleading_jet =  ak.where(nrand == 1,  leading_jet, subleading_jet)
    
    return assigned_leading_jet, assigned_subleading_jet

# Function to calculate pT balance

def pT_balance(leading_jet_pt, subleading_jet_pt):
    pT_b = abs((leading_jet_pt - subleading_jet_pt)/(leading_jet_pt + subleading_jet_pt)) 
    return pT_b

# Function to calculate pT_average_cosh(eta_subleading_jet)

def pTave_cosh_eta(leading_jet_pt, subleading_jet_pt)
    pTave_c_eta_sub = 0.5*(leading_jet_pt + subleading_jet_pt)
    return pTave_c_eta_sub

# Function to calculate delta_phi

def delta_phi(leading_jet_phi, subleading_jet_phi):
    d_phi = abs(leading_jet_phi - subleading_jet_phi)
    return d_phi
