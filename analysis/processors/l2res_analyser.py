import os, sys

import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, BaseSchema
from coffea import processor, hist, util
from coffea.nanoevents.methods import vector
from coffea.nanoevents.methods.vector import ThreeVector

import uproot4

import array
import json
import numpy as np

from processors import l2res 

class L2ResProcessor(processor.ProcessorABC):
  def __init__(self, jet_size, jet_type):
     
    self._jetsize = jet_size
    self._jettype = jet_type

    alpha_bins = [0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45]
    eta_bins = [0, 0.261, 0.522, 0.783, 1.044, 1.305, 1.479, 1.653, 1.93,
        2.172, 2.322, 2.5, 2.65,
        2.853, 2.964, 3.139, 3.489, 3.839, 5.191] 
    
    self._histo1 = hist.Hist(
        "Events",
        hist.Cat("dataset", "Dataset"),
        hist.Cat("method", "Method"),
        hist.Cat("collection", "Collection"),
        hist.Bin("alpha", "#alpha", alpha_bins),
        hist.Bin("eta_probe", "#|eta^{probe}|", eta_bins),
        hist.Bin("dijet_ptav", "Dijet average transverse momentum: p^{avw}_{T,dijet} [GeV]", [15, 65, 89, 111, 156, 1000]),
        hist.Bin("balance", "Balance", 100, -1., 1.)
    )

    self._accumulator = processor.dict_accumulator({
      'histo1' : self._histo1,
    })

    self._jet_triggers = [
        #("ZeroBias", 0),
        #("PFJet40" , 40 ),
        #("PFJet60" , 60 ),
        #("PFJet80" , 80 ),
        #("PFJet140", 140),
        #("PFJet200", 200),
        #("PFJet260", 320), 
        #("PFJet320", 320),
        #("PFJet400", 400),
        #("PFJet450", 450),
        ("PFJet500", 500) 
    ]

  @property
  def accumulator(self):
    return self._accumulator

    # we wiill receive a NanoEvents instead of a coffeea DataFrame
  def process(self, events):
      out = self.accumulator.identity()

      nevents = np.zeros(len(events))
      
      # Take trigger path from _jet_trigger
      trigger_paths = list(list(zip(*self._jet_triggers))[0])
      # take nominal value of trigger from _jet_trigger
      trigger_nominal = list(list(zip(*self._jet_triggers))[1])

      # check if any of the triggers is fired
      triggers = np.zeros(len(events), dtype=np.bool)
      for path in trigger_paths:
        if path not in events.HLT.fields: continue
        triggers = triggers | events.HLT[path]

      #take events that fire the triggers
      events=events[triggers]
     
      jettype = ['chs', 'puppi'] if self._jettype=="all" else [self._jettype]
      jetsize = [0.4, 0.8] if self._jetsize=="all" else [float(self._jetsize)]
      
      for jtype in jettype:
        for jsize in jetsize: 
          # get dict generated with l2resSelection method
          eventsdict  = l2res.l2resSelection(events, jsize, jtype)
          tag_jet     = eventsdict['tag_jet']
          probe_jet   = eventsdict['probe_jet']
          
          out['histo1'].fill(
            dataset=events.metadata["dataset"],
            method =  "MPFBalance",
            collection = eventsdict['collection'],
            alpha = eventsdict['alpha'],
            dijet_ptav = (tag_jet.pt + probe_jet.pt)/2,
            eta_probe = probe_jet.eta,
            balance = eventsdict['mpf_balance']
          )
          out['histo1'].fill(
            dataset=events.metadata["dataset"],
            method =  "PTBalance",
            collection = eventsdict['collection'],
            alpha = eventsdict['alpha'],
            dijet_ptav = (tag_jet.pt + probe_jet.pt)/2,
            eta_probe = probe_jet.eta,
            balance = eventsdict['pt_balance']
          )
          del eventsdict, tag_jet, probe_jet
      return out
    
  def postprocess(self, accumulator):
        return accumulator
