import os, sys

import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, BaseSchema
from coffea import processor, hist, util
from coffea.nanoevents.methods import vector
from coffea.nanoevents.methods.vector import ThreeVector

import array
import json
import numpy as np

#from processors import config

# Based on decafMET gammaJes.py
# https://github.com/dvannerom/decafMET/blob/master/decafMET/analysis/processor/gammaJets.py

def emulationSelection(events, phase_space):

  # Make an entry in events for hlt jets
  events["HLTJet"] = events.TrigObj[ak.where(events.TrigObj.id==1,True, False)]
  
  # Define jet selection
  selectedJets = events.Jet
  selectedJets = selectedJets[(events.Jet.isTight)]

  # Define HLT jet selection
  selectedHLTJets = events.HLTJet

  # Take events that contain 2 or more HLT jets and jets
  events =  events[(ak.num(selectedJets)>=phase_space.min_njets)  & (ak.num(selectedHLTJets)>=phase_space.min_njets)]

  # Define alpha
  njets = ak.num(events.Jet)
  alpha = np.zeros(len(events))
  alpha[njets>2] = 0.5 * events[njets>2].Jet[:,2].pt / (events[njets>2].Jet[:,0].pt + events[njets>2].Jet[:,1].pt)
  
  # Take events with alpha < 0.3
  events = events[alpha < phase_space.max_alpha]
  
  # Define leading jet Lorentz vector
  leadingPFJet = events.Jet[:,0]
  leadingPFJetLorentz = ak.zip(
      {
          "pt"  : leadingPFJet.pt,
          "eta" : leadingPFJet.eta,
          "phi" : leadingPFJet.phi,
          "mass": leadingPFJet.mass,
      },
      with_name="PtEtaPhiMLorentzVector"
  )

  # Define sub-leading jet Lorentz vector
  subleadingPFJet = events.Jet[:,1]
  subleadingPFJetLorentz = ak.zip(
      {
          "pt"  : subleadingPFJet.pt,
          "eta" : subleadingPFJet.eta,
          "phi" : subleadingPFJet.phi,
          "mass": subleadingPFJet.mass,
      },
      with_name="PtEtaPhiMLorentzVector"
  )

  # Define leading jet Lorentz vector
  leadingHLTJet = events.HLTJet[:,0]
  leadingHLTJetLorentz = ak.zip(
      {
          "pt"  : leadingHLTJet.pt,
          "eta" : leadingHLTJet.eta,
          "phi" : leadingHLTJet.phi,
          "mass": 0,
      },
      with_name="PtEtaPhiMLorentzVector"
  )

  # Define sub-leading jet Lorentz vector
  subleadingHLTJet = events.HLTJet[:,1]
  subleadingHLTJetLorentz = ak.zip(
      {
          "pt"  : subleadingHLTJet.pt,
          "eta" : subleadingHLTJet.eta,
          "phi" : subleadingHLTJet.phi,
          "mass": 0,
      },
      with_name="PtEtaPhiMLorentzVector"
  )
  
  # Compute delta R between HLT and PF jets
  deltaR_hltjet0_pfjet0 = leadingHLTJetLorentz.delta_r(leadingPFJetLorentz)
  deltaR_hltjet0_pfjet1 = leadingHLTJetLorentz.delta_r(subleadingPFJetLorentz)
  deltaR_hltjet1_pfjet0 = subleadingHLTJetLorentz.delta_r(leadingPFJetLorentz)
  deltaR_hltjet1_pfjet1 = subleadingHLTJetLorentz.delta_r(subleadingPFJetLorentz)

  # Match jets with deltaR < 0.4
  is_hltjet0_matched = (deltaR_hltjet0_pfjet0< phase_space.max_dr_hlt_pf_jet) | (deltaR_hltjet0_pfjet1< phase_space.max_dr_hlt_pf_jet)
  is_hltjet1_matched = (deltaR_hltjet1_pfjet0< phase_space.max_dr_hlt_pf_jet) | (deltaR_hltjet1_pfjet1< phase_space.max_dr_hlt_pf_jet)

  # Define delta phi cut
  deltaphi_jet0_jet1 = abs(leadingPFJetLorentz.phi - subleadingPFJetLorentz.phi)

  # One of the jets has to be central
  is_central = (abs(leadingPFJetLorentz.eta)< phase_space.max_abseta_centraljet)  | (abs(subleadingPFJetLorentz.eta)<phase_space.max_abseta_centraljet)

  # apply cuts to the event
  events = events[(is_central) & (is_hltjet0_matched) & (is_hltjet1_matched) & (deltaphi_jet0_jet1>phase_space.min_dphi_leadingjets)]

  # is forward event if one of the leading jets has an abs(eta) > 2.853
  is_forward = (abs(events.Jet[:,1].eta)> phase_space.min_abseta_forwardjet) | (abs(events.Jet[:,0].eta)> phase_space.min_abseta_forwardjet)  
  forward_events = events[is_forward]

  # define forward and central PF jet
  forwardjet = ak.where(abs(forward_events.Jet[:,1].eta) < phase_space.min_abseta_forwardjet, forward_events.Jet[:,0], forward_events.Jet[:,1])
  centraljet = ak.where(abs(forward_events.Jet[:,1].eta) < phase_space.min_abseta_forwardjet, forward_events.Jet[:,1], forward_events.Jet[:,0])

  # define forward and central HLT jet
  forwardHLTjet = ak.where(abs(forward_events.HLTJet[:,1].eta)<2.4, forward_events.HLTJet[:,0], forward_events.HLTJet[:,1])
  centralHLTjet = ak.where(abs(forward_events.HLTJet[:,1].eta)<2.4, forward_events.HLTJet[:,1], forward_events.HLTJet[:,0])

  # define lorentz vector for forward jet
  forwardjetLorentz =ak.zip(
      {
          "pt"  : forwardjet.pt,
          "eta" : forwardjet.eta,
          "phi" : forwardjet.phi,
          "mass": forwardjet.mass,
      },
      with_name="PtEtaPhiMLorentzVector"
  )
  
  # define lorentz vector for central jet
  centraljetLorentz =ak.zip(
      {
          "pt"  : centraljet.pt,
          "eta" : centraljet.eta,
          "phi" : centraljet.phi,
          "mass": centraljet.mass,
      },
      with_name="PtEtaPhiMLorentzVector"
  )

  forwardHLTjetLorentz =ak.zip(
      {
          "pt"  : forwardHLTjet.pt,
          "eta" : forwardHLTjet.eta,
          "phi" : forwardHLTjet.phi,
          "mass": 0,
      },
      with_name="PtEtaPhiMLorentzVector"
  )

  # define lorentz vector for central jet
  centralHLTjetLorentz =ak.zip(
      {
          "pt"  : centralHLTjet.pt,
          "eta" : centralHLTjet.eta,
          "phi" : centralHLTjet.phi,
          "mass": 0,
      },
      with_name="PtEtaPhiMLorentzVector"
  )
  
  # match forward and central PF and HLT jets
  is_forward_matched = forwardHLTjetLorentz.delta_r(forwardjetLorentz) < phase_space.max_dr_hlt_pf_jet 
  is_central_matched = centralHLTjetLorentz.delta_r(centraljetLorentz) < phase_space.max_dr_hlt_pf_jet

  # define forward events
  forward_events =  forward_events[(is_forward_matched) & (is_central_matched)]
 
  emulationDict = {'events': events, 'forward_events': forward_events}
  
  return emulationDict
