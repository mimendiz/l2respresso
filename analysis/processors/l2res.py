import os, sys

import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, BaseSchema
from coffea import processor, hist, util
from coffea.nanoevents.methods import vector
from coffea.nanoevents.methods.vector import ThreeVector

import array
import json
import numpy as np

# Based on decafMET gammaJes.py
# https://github.com/dvannerom/decafMET/blob/master/decafMET/analysis/processor/gammaJets.py

def pt_balance(tjet, pjet):

  # this function computes the pt balance 
  return (pjet.pt - tjet.pt) / (pjet.pt + tjet.pt)

def mpf_balance(tjet, pjet, met):

  # this function computes mpf balance 
  tjet_pt = [np.cos(tjet.phi), np.sin(tjet.phi)]
  met_pt  = [met.pt*np.cos(met.phi), met.pt*np.sin(met.phi)]
  dot_product = tjet_pt[0]*met_pt[0] + tjet_pt[1]*met_pt[1]
  return  dot_product/ (pjet.pt + tjet.pt)

def l2resSelection(events, rvalue, jettype):

  # Define jet collection with size and jet type
  jet_collection = 'Jet' if rvalue==0.4 else 'FatJet' if rvalue==0.8 else print('Jet size does not exist')
  if rvalue == 0.4:
    jet_collection += '' if jettype=="chs" else 'Puppi'
  elif rvalue == 0.8:
    jet_collection += '' if jettype=="puppi" else 'CHS'

  # Add jet collection to dataset for easier handling
  selected_jets = events[jet_collection]

  # make sure they are sorted by transverse momentum
  selected_jets = selected_jets[ak.argsort(selected_jets.pt, axis=1, ascending=False)]
  selected_jets = selected_jets[selected_jets.isTight]
  events['JetCol']= selected_jets

  # Take events that contain 2 or more jets and jets with tight ID
  events =  events[(ak.num(events.JetCol)>=2)]

  # Define alpha
  njets = ak.num(events.JetCol)
  alpha = np.zeros(len(events))
  alpha[njets>2] = 0.5 * events[njets>2].JetCol[:,2].pt  / (events[njets>2].JetCol[:,0].pt + events[njets>2].JetCol[:,1].pt)
  events['alpha'] = alpha

  
  # Define leading jet 
  ljet = events.JetCol[:,0]

  # Define sub-leading jet Lorentz vector
  sjet = events.JetCol[:,1]

  # One of the jets has to be central
  is_central = (abs(ljet.eta)<1.3) | (abs(sjet.eta)<1.3)

  # dijet dif smaller than 0.7
  dijet_dif = (ljet.pt-sjet.pt)/(ljet.pt + sjet.pt)
  
  # delta phi of leading jets
  dphi = ljet.phi-sjet.phi

  # apply cuts to the event
  events = events[(is_central) & (abs(dijet_dif)<0.7) & (abs(dphi)>2.7)]

  # define tag jet as the central jet, if both jets are central jets
  # randomly select oone of the jets as tag

  # generate randomly 1 or 0, this will tell us which jet is the tagged one
  rand_tag = np.random.randint(2, size=len(events))

  # define the first and second jet
  firstjet = events.JetCol[:,0]
  secondjet= events.JetCol[:,1]

  # get in which events both jets are central
  both_central = (abs(firstjet.eta)<1.3) & (abs(secondjet.eta)<1.3)

  # tag jet: if both central take from random tag which one is the tag jet
  tag_jet = ak.where(both_central, ak.where(rand_tag==0, firstjet, secondjet), ak.where(abs(firstjet.eta)<1.3, firstjet, secondjet))

  # probe jet: if both central take from random tag which one is not the tag jet
  probe_jet = ak.where(both_central,
      ak.where(rand_tag==0, secondjet, firstjet),
      ak.where(abs(firstjet.eta)<1.3, secondjet, firstjet))
  
  # get response with pt balance method
  pt_resp = pt_balance(tag_jet, probe_jet)

  # get the corresponding met to the jet type
  met = events['ChsMET'] if jettype=="chs" else events['PuppiMET']

  # get response with mpf method
  mpf_resp = mpf_balance(tag_jet, probe_jet, met)

  # outname 
  jet_name = "AK" 
  jet_name += "4" if rvalue==0.4 else "8" if rvalue==0.8 else ''
  jet_name += "PF"+jettype

  l2resDict = {'alpha': events.alpha, 'tag_jet': tag_jet, 'probe_jet': probe_jet, 'met': met, 'mpf_balance': mpf_resp, 'pt_balance' : pt_resp, "collection": jet_name}
  
  return l2resDict
