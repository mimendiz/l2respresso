import os, sys

import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, BaseSchema
from coffea import processor, hist, util
from coffea.nanoevents.methods import vector
from coffea.nanoevents.methods.vector import ThreeVector

import uproot4

import array
import json
import numpy as np
from processors import emulationL2Res, l2res, tagandprobeL2Res, config 

class L2ResProcessor(processor.ProcessorABC):
  def __init__(self):
    self._histo1 = hist.Hist(
        "Events",
        hist.Cat("dataset", "Dataset"),
        hist.Cat("method", "method"),
        hist.Cat("trigger", "trigger"),
        hist.Cat("fraction", "fraction"),
        hist.Bin("jet_pt", "Jet transverse momentum: p_{T} [GeV]", 120, 0., 1200),
    )
  
    self._accumulator = processor.dict_accumulator({
      'histo1' : self._histo1,
    })

    # Load triggers from config file
    configuration = config.Config()
    self._jet_triggers = configuration.central_triggers

    # Add zero bias trigger
    self._jet_triggers.insert(0, ('ZeroBias', 0))

    # load phase space
    self.phase_space = configuration.phase_space_trigger_efficiency

  @property
  def accumulator(self):
    return self._accumulator

    # we wiill receive a NanoEvents instead of a coffeea DataFrame
  def process(self, events):
      out = self.accumulator.identity()

      print(self.phase_space)
      nevents = np.zeros(len(events))
      
      # Take trigger path from _jet_trigger
      trigger_paths = list(list(zip(*self._jet_triggers))[0])
      # take nominal value of trigger from _jet_trigger
      trigger_nominal = list(list(zip(*self._jet_triggers))[1])

      # check if any of the triggers is fired
      triggers = np.zeros(len(events), dtype=np.bool)
      for path in trigger_paths:
        if path not in events.HLT.fields: continue
        triggers = triggers | events.HLT[path]

      #take events that fire the triggers
      events=events[triggers]
      
      # get dict generated from emulation method
      eventsdict_emulation = emulationL2Res.emulationSelection(events, self.phase_space)
      emulationevents = eventsdict_emulation['events']

      # get dictionary for tag and probe method
      eventsdict_tagandprobe = tagandprobeL2Res.tagandprobeSelection(events, self.phase_space)
      tag_probe_events = eventsdict_tagandprobe['events']
 
      for i in range(1, len(trigger_paths)):
        # fill denominator with events that fire reference trigger
        print(trigger_paths[i-1])
        events_fill = emulationevents[emulationevents.HLT[trigger_paths[i-1]]]
        out['histo1'].fill(
          method = 'Emulation',
          dataset=events.metadata["dataset"],
          trigger =  trigger_paths[i],
          fraction = "Den",
          jet_pt = events_fill.Jet[:,0].pt
        )

        # fill numerator when the HLT jet pt is larger than jet threshold
        threshold = trigger_nominal[i]  
        out['histo1'].fill(
          method = 'Emulation',
          dataset=events.metadata["dataset"],
          trigger =  trigger_paths[i],
          fraction = "Num",
          jet_pt = events_fill[ events_fill.HLTJet[:,0].pt > threshold].Jet[:,0].pt
        )
        
        events_fill = tag_probe_events[tag_probe_events.TagHLTJet.pt > threshold]
        out['histo1'].fill(
            method = 'Tag&Probe',
            dataset=events.metadata["dataset"],
            trigger =  trigger_paths[i],
            fraction = "Den",
            jet_pt = events_fill.ProbePFJet.pt
        )

        out['histo1'].fill(
            method = 'Tag&Probe',
            dataset=events.metadata["dataset"],
            trigger =  trigger_paths[i],
            fraction = "Num",
            jet_pt = events_fill[events_fill.ProbeHLTJet.pt > threshold].ProbePFJet.pt
        )

      return out
    
  def postprocess(self, accumulator):
        return accumulator
