import os, sys

import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, BaseSchema
from coffea import processor, hist, util

import uproot

import time

from hist import Hist
import hist


from optparse import OptionParser

from processors import l2res_analyser

parser = OptionParser()
parser.add_option('-t', '--jettype', help='jet reconstruction, chs or puppi', dest='jettype')
parser.add_option('-s', '--jetsize', help='R parameter for reconstruction',   dest='jetsize')
parser.add_option('-w', '--workers', help='Number of workers to use for multi-worker executors (e.g. futures or condor)', dest='workers', type=int,
    default=8)
(options, args) = parser.parse_args()

fileset = {
    'JetHT':[
        #'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/250000/1F2D4C88-2F82-4A43-BF66-A8D59A7EC5E8.root',
        #'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/250000/65AE7EB1-FF3F-EB42-A16D-71A9926F4A6A.root',
        #'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/2510000/4CA7AA2C-351D-D943-9A19-91D445B57DDA.root',
        #'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/2510000/FF229678-C304-7B41-AF85-F523D111B38A.root',
        #'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/2510000/9BE5EDFF-77E8-5548-982F-42AB030F125E.root',
        #'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/2510000/343F665A-CD3B-EB43-81BF-2F29D0A40E5D.root',
        ##'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/2510000/69F064BE-C187-4F4D-9B9E-B6AACB0A351C.root',
        #'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/2510000/D14E5F52-E711-D24E-B7F3-101332A0C5A5.root',
        #'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/2510000/D79D884F-15FE-6F47-9ACA-64692DC12C94.root',
        #'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/2510000/5AE1E3B6-CFCC-FF4D-B70F-671EF630AAFB.root',
        #'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/2510000/B16FF6DE-EC37-C042-86EE-31E43984BF0E.root',
        #'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/2510000/AFE9F2F2-CA08-C242-A977-8BFC34C21544.root',
        #'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/2510000/F9106727-0FE0-1C48-92F5-6BF6F55FEFB8.root',
        #'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/2510000/45B1185A-2FE6-AD40-B36F-BEE045DA70C5.root',
        #'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_80to120_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/40000/F333A55E-D247-8742-85CC-53399AAA835A.root',
        #'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_80to120_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/40000/7979DA41-A671-7646-A5A2-43E872DB43AD.root',
        #'/pnfs/desy.de/cms/tier2/store/mc/RunIISummer20UL17NanoAODv9/QCD_Pt_80to120_TuneCP5_13TeV_pythia8/NANOAODSIM/20UL17JMENano_106X_mc2017_realistic_v9-v1/40000/2F74C3E1-998B-8942-BB5B-DC71FCBFB2E9.root'
        #'/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/UL2016_MiniAODv2_JMENanoAODv9-v1/2430000/7E050CC8-BC9A-A447-8809-216421F59AE9.root',
        #'/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/HIPM_UL2016_MiniAODv2_JMENanoAODv9-v1/2430000/A1CFE3CE-A38B-3D48-971D-AA587CBD2B33.root',
        #'/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/UL2016_MiniAODv2_JMENanoAODv9-v1/100000/05F01478-1CBE-7345-A044-849830D774FE.root',
        #'/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/UL2016_MiniAODv2_JMENanoAODv9-v1/100000/42D50B42-F751-FB48-92A5-7C2CD67738E6.root',
        #'/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/UL2016_MiniAODv2_JMENanoAODv9-v1/100000/75C51A2C-8590-AD42-A2B1-1F7CFA8E221B.root',
        #'/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/UL2016_MiniAODv2_JMENanoAODv9-v1/100000/A53ABF89-0023-5F45-8110-6AAED28AC4E6.root',
        #'/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/UL2016_MiniAODv2_JMENanoAODv9-v1/100000/EC336ADF-1E04-F04E-BD53-2BA3660DD1CF.root',
        #'/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/UL2016_MiniAODv2_JMENanoAODv9-v1/100000/0C10531F-8212-4B4B-9D2A-272AC2D16C24.root',
        '/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/UL2016_MiniAODv2_JMENanoAODv9-v1/100000/44E4F698-9893-6949-93DB-D36206D382F7.root',
        '/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/UL2016_MiniAODv2_JMENanoAODv9-v1/100000/88CAB594-054D-214B-BBCB-62919E07A000.root',
        '/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/UL2016_MiniAODv2_JMENanoAODv9-v1/100000/A5EEFC3F-8E5C-654F-81D6-B6DD16B9AA58.root',
        '/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/UL2016_MiniAODv2_JMENanoAODv9-v1/100000/15B66BF3-AAC1-1B46-BF95-2839FEDC737F.root',
        '/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/UL2016_MiniAODv2_JMENanoAODv9-v1/100000/52E9B895-5975-BC41-ACD3-D91C7391705A.root',
        '/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/UL2016_MiniAODv2_JMENanoAODv9-v1/100000/8E1903A1-9CFC-3A4D-BF52-2F1D80AD1025.root',
        '/pnfs/desy.de/cms/tier2/store/data/Run2016F/JetHT/NANOAOD/UL2016_MiniAODv2_JMENanoAODv9-v1/100000/A6AFAAF1-322A-1741-8589-FACC0FA6AEA2.root',
    ]
}

print(options.jetsize, options.jettype)

out = processor.run_uproot_job(
    fileset,
    "Events",
    l2res_analyser.L2ResProcessor(options.jetsize, options.jettype),
    executor=processor.futures_executor,
    executor_args={
      "schema": NanoAODSchema, "workers": 6,
    },
    chunksize=10000,
)


fout = uproot.recreate('L2ResBalance.root')

histogram = out['histo1'].to_hist()
method = histogram.axes['method']
eta_bins = histogram.axes['eta_probe']
pt_bins = histogram.axes['dijet_ptav']
alpha_bins = histogram.axes['alpha']
print (histogram.axes['collection'])

for jet in histogram.axes['collection']:
  hcollection = histogram[{"collection": jet}]
  for m in method:
    print(m)
    hmethod = hcollection[{"method":m }]
    for i in range(len(eta_bins)):
      etaname = "EtaLow"+str(eta_bins[i][0])+"EtaHigh"+str(eta_bins[i][1])
      for j in range(len(pt_bins)):
        ptname = "PtLow"+str(pt_bins[j][0])+"PtHigh"+str(pt_bins[j][1])
        for k in range(len(alpha_bins)):
          alphaname = "AlphaLow"+str(alpha_bins[k][0])+"AlphaHigh"+str(alpha_bins[k][1])
          hbalance = hmethod[{"eta_probe": i , "dijet_ptav": j, "alpha": k}]
          hbalance = hbalance.project("balance")
          fout[jet+"/h"+m+etaname+ptname+alphaname]=hbalance
    print(m, "wrote")
     

fout.close()
