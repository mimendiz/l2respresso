import uproot as up
import coffea as cof
import awkward as ak
import platform

if __name__ == '__main__':
    print("akward: ", ak.__version__)
    print("coffea: ", cof.__version__)
    print("uproot: ", up.__version__)
    print("python: ", platform.python_version())
